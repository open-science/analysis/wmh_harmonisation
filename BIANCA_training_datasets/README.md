# FSL-BIANCA training datasets

This directory contains training datasets for FSL-BIANCA created to produce harmonised WMH segmentations, as described in our paper:
> "Integrating large-scale neuroimaging research datasets: harmonisation of white matter hyperintensity measurements across Whitehall and UK Biobank datasets"
>
> V Bordin, I Bertani, I Mattioli, V Sundaresan, P McCarthy, S Suri, E Zsoldos, N Filippini, A Mahmood, L Melazzini, MM Laganà, G Zamboni, A Singh-Manoux, M Kivimäki, KP Ebmeier, G Baselli, M Jenkinson, CE Mackay, E Duff*, L Griffanti*. 


## How to cite
Our paper is available [here](https://www.sciencedirect.com/science/article/pii/S1053811921004663?via%3Dihub)

Preprint version [here](https://www.biorxiv.org/content/10.1101/2020.07.28.208579v1)

## Contents

*  `Mixed_WH_FLAIR_T1_FA` and `Mixed_WH_FLAIR_T1_FA_labels` is a training file generated using data from 24 Whitehall participants scanned on a 3T Siemens Verio (WH1) and 24 Whitehall participants scanned on a 3T Siemens Prisma (WH2). Uses intensity features from FLAIR, T1-weighted and Fractional Anisotropy.
*  `Mixed_WH_FLAIR_T1` and `Mixed_WH_FLAIR_T1_labels` is a training file generated using data from 24 Whitehall participants scanned on a 3T Siemens Verio (WH1) and 24 Whitehall participants scanned on a 3T Siemens Prisma (WH2). Uses intensity features from FLAIR and T1-weighted images.
*  `Mixed_WH-UKB_FLAIR_T1` and `Mixed_WH-UKB_FLAIR_T1_labels` is a training file generated using data from 24 Whitehall participants scanned on a 3T Siemens Verio (WH1), 24 Whitehall participants scanned on a 3T Siemens Prisma (WH2) and 12 UK Biobank participants scanned on a 3T Siemens Skyra (UKB)*. Uses intensity features from FLAIR and T1-weighted images.

*  `Sequences_details.pdf` contains the details of the sequences used in the 3 datasets


Other parameters used to generate the training files (kept constant for all training options):
2,000 training points for lesion class, 10,000 points for the non-lesion class, patch size=3 voxels, spatial weighting=2.
T1 and FLAIR images were brain-extracted and bias field corrected. FLAIR images were further masked using a white matter mask (created with [make_bianca_mask](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BIANCA/Userguide#Masking))

*Same participants of the BIANCA training set used in the [UKB imaging pipeline](https://www.fmrib.ox.ac.uk/ukbiobank/fbp/)

## Usage
1. Download/save the file pair in the same location (<training_dir>)
2. Create your BIANCA masterfile following the format specified below
3. Use the path and basename of the training file with the `--loadclassifierdata` option in your BIANCA call (examples below).

For more details on FSL-BIANCA and all the required steps, please refer to the [BIANCA wiki page](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BIANCA)

**Mixed Whitehall FLAIR+T1+FA**
*  Masterfile format:
`<subN>/FLAIR_brain_restore_masked.nii.gz <subN>/T1_biascorr_brain_2_FLAIR.nii.gz <subN>/WMH_mask.nii.gz <subN>/FLAIR_2_MNI.mat <subN>/FA_2_FLAIR.nii.gz`

(the column for WMH_mask can be replaced with any 'placehold' text)

* Example call:
`bianca --singlefile=masterfile.txt --brainmaskfeaturenum=1 --querysubjectnum=<num> --featuresubset=1,2,5 --matfeaturenum=4 --spatialweight=2 --patchsizes=3 --loadclassifierdata=<training_dir>/Mixed_WH_FLAIR_T1_FA -o <outname> –v`

**Mixed Whitehall FLAIR+T1**
*  Masterfile format:
`<subN>/FLAIR_brain_restore_masked.nii.gz <subN>/T1_biascorr_brain_2_FLAIR.nii.gz <subN>/WMH_mask.nii.gz <subN>/FLAIR_2_MNI.mat`

(the column for WMH_mask can be replaced with any 'placehold' text)

* Example call:
`bianca --singlefile=masterfile.txt --brainmaskfeaturenum=1 --querysubjectnum=<num> --featuresubset=1,2 --matfeaturenum=4 --spatialweight=2 --patchsizes=3 --loadclassifierdata=<training_dir>/Mixed_WH_FLAIR_T1 -o <outname> –v`

**Mixed Whitehall+UKB - FLAIR+T1**
*  Masterfile format:
`<subN>/FLAIR_brain_restore_masked.nii.gz <subN>/T1_biascorr_brain_2_FLAIR.nii.gz <subN>/WMH_mask.nii.gz <subN>/FLAIR_2_MNI.mat`

(the column for WMH_mask can be replaced with any 'placehold' text)

* Example call:
`bianca --singlefile=masterfile.txt --brainmaskfeaturenum=1 --querysubjectnum=<num> --featuresubset=1,2 --matfeaturenum=4 --spatialweight=2 --patchsizes=3 --loadclassifierdata=<training_dir>/Mixed_WH-UKB_FLAIR_T1 -o <outname> –v`
