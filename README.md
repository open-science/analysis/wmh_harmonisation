# WMH_Harmonisation

Code for DPUK Discovery Award:
> "Harmonising large-scale imaging databases to provide integrated assessments of the role of white matter hyperintensities in
cognitive aging."

Results presented in the paper:
> "Integrating large-scale neuroimaging research datasets: harmonisation of white matter hyperintensity measurements across Whitehall and UK Biobank datasets"
>
> V Bordin, I Bertani, I Mattioli, V Sundaresan, P McCarthy, S Suri, E Zsoldos, N Filippini, A Mahmood, L Melazzini, MM Laganà, G Zamboni, A Singh-Manoux, M Kivimäki, KP Ebmeier, G Baselli, M Jenkinson, CE Mackay, E Duff*, L Griffanti*. 


## How to cite
Our paper is available [here](https://www.sciencedirect.com/science/article/pii/S1053811921004663?via%3Dihub)

Preprint version [here](https://www.biorxiv.org/content/10.1101/2020.07.28.208579v1)

## Contents
**Non-imaging data harmonisation**
*  `funpack_wmh_bb` - Code to align non-imaging data from UK Biobank and Whitehall-II imaging sub-study.

**Imaging data harmonisation**
*  `BIANCA_training_datasets` - Training files for FSL-BIANCA for harmonised segmentation of white matter hyperintenisities in UK Biobank and Whitehall-II imaging sub-study.

Further details are provided in the README.md files within each directory
