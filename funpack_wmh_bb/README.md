# WHITEHALL - BIOBANK PARSER

This is a brief manual to align non-imaging data from UK Biobank and Whitehall-II imaging sub-study.
The parser uses Paul McCarthy's [FUNPACK](https://git.fmrib.ox.ac.uk/fsl/funpack/).

At present the parser applies to the variables used in our paper:
> "Integrating large-scale neuroimaging research datasets: harmonisation of white matter hyperintensity measurements across Whitehall and UK Biobank datasets"
>
> V Bordin, I Bertani, I Mattioli, V Sundaresan, P McCarthy, S Suri, E Zsoldos, N Filippini, A Mahmood, L Melazzini, MM Laganà, G Zamboni, A Singh-Manoux, M Kivimäki, KP Ebmeier, G Baselli, M Jenkinson, CE Mackay, E Duff*, L Griffanti*. 

However, it is fully customizable, so it can be adapted to different datasets and expanded to include more variables and conversion rules.  

## How to cite
Our paper is available [here](https://www.sciencedirect.com/science/article/pii/S1053811921004663?via%3Dihub)

Preprint version [here](https://www.biorxiv.org/content/10.1101/2020.07.28.208579v1)


If you would like to cite FUNPACK, please refer to its [Zenodo page](https://zenodo.org/record/3761702#.XwSnvC0ZN24).


## Contents

1. `Parser.xlsx` is the excel file with the matching variables between the 2 datasets, all the notes and rules used in our study:

[OX.ID,OX.AGE,GenderM1F0,OX.WEIGHT,OX.HEIGHT,OX.BMI,OX.BP_SYS,OX.BP_DIA,OX.PULSE,ModeratePA_h/week,VigorousPA_h/week,CHAMwalk_h/week,TVtot_h/week,TotWalk_h/week,SleepDuration,HealthClasses,SmokerStatus_C-NC,Cig/day,AlcoholStatus_C-NC,AlcoholU/w,OX.MEDS_TS_ALL,BNF_CVDmedY1N0,BNF_AntideprY1N0,BPMedRawY1N0,DiabRawY1N0,CVDRawY1N0,CESD_depressed,OX.EDUC_FT_END,HandClasses1R2L3A,TMT_A_s,TMT_B_s,OX.DSB,OX.DCOD,OX.CANTAB_SIM_RT_MEAN,CSF,CSFpercent,GM,WM,WholeBrain,WMH_tot,WMH_percentBV]

2. `conversion.py` is the python file with new conversion functions not included in the FUNPACK library as:

- meters() : conversion from meters to centimeters 
- TV(): conversion from h/day to h/week  ( in this case spent to watch TV)
- TotWalk(): conversion from min/day to h/week  ( in this case of walking)
- alcohol(): product of the two variables that stand for unit/day and day/week
- ChamConvert(): product of the two variables that stand for min/day and day/week, all divided by 60 to finally have h/week( using for the various Physical Activity)
- WMH(): to get white matter hyperintensities percentage from the Whole Brain volume and the WMH total volume

3. `CONFIGURATION.txt` CONFIGURATION file for automated conversion to Whitehall format.


## Usage

**Requirements:** [FUNPACK](https://git.fmrib.ox.ac.uk/fsl/funpack/) installed in a Python 3 environment.  An example installation using conda python environment is given below.  

### MANUAL CONVERSION

1.  Create a virtual environment as : `conda create -p ./funpack_env python=3.6`   (just do this once, subsequently : `source activate funpack_env`


2.  Download and install FUNPACK  in your virtual environment from:
[https://git.fmrib.ox.ac.uk/fsl/funpack](https://git.fmrib.ox.ac.uk/fsl/funpack).
The `funpack_demo` is a very useful Jupyter Notebook which introduces
the main features provided by funpack. 

3. Import UK Biobank variables (columns) of interest. The visit is specified with 0.0 or 2.0 (42 variables):

`funpack -q -ow -co 20159-0.0 -co 20156-0.0 -co 20157-0.0 -co 20403-0.0 -co 20414-0.0 -co 20546-0.2 -co 20510-0.0 -co 20240-0.0 -co 1707-0.0 -co 845-0.0 -co 31-0.0 -co 21002-2.0 -co 21001-2.0 -co 4080-2.0 -co 4079-2.0 -co 1160-2.0 -co 137-2.0 -co 102-2.0 -co 25004-2.0 -co 25003-2.0 -co 25006-2.0 -co 25008-2.0 -co 25010-2.0 -co 25781-2.0 -co 3456-2.0 -co 21003-2.0 -co 20023-2.0 -co 884-2.0 -co 894-2.0 -co 904-2.0 -co 914-2.0 -co 864-2.0 -co 874-2.0 -co 12144-2.0 -co 1070-2.0 -co 2178-2.0 -co 20116-2.0 -co 20117-2.0  -co 6177-2.0  -co 2443-2.0 -co 6150-2.0  -co 2100-2.0  output.csv ukb26225.csv`

(where `ukb26225.csv` is your UK Biobank dataset)

4. Clean columns for unknown response or missing data (most of them are already inside the builtins options, some others can be added, as for "-7" for variable 6177):

`funpack  -q -ow -nv 2178 "-3" -nv 20116  " -3" -nv 20117  " -3" -nv 6177   " -1,-3,-7" -nv 20546  " -818" -nv 2443   " -1,-3" -nv 6150   " -3" -nv 20510  " -818" -nv 1707   " -3" -nv 2100   " -818" -nv 20403  " -818" -nv 20414  " -818" output.csv output.csv `


5. Fill missing data with 0 in quantitative variables (e.g. how many cigarettes) where the answer to the generic question was no (e.g. Do you smoke?):           

`funpack -q -ow -cv 914 "v904 ==0" "0" -cv 894 "v884 ==0" "0" -cv 874 "v864 ==0" "0" -cv 20403 "v20414 ==0" "0"  output.csv output.csv` 


6. Recode categorical variables to match the Whitehall coding (see file Parser.xlsx with detailed notes):

`funpack -q -ow -re 2178 "1,2,3,4"  "4,3,2,1" -re 20116 "2" "1" -re 20117 "2" "1" -re 20546 "3,1,4"  "1,0,0" -re 6150 "2,3,4,-7"  "1,1,1,0" -re 20403 "1,2,3,4,5" "1.5,3.5,5.5,8,10" -re 20414 "1,2,3,4" "0.25, 0.75, 2.5, 4" output.csv output.csv `

7. Split column into a separate column for each category (e.g. you might want to extract just 1 for cardiovascular medication and 2 for Blood pressure medications):

`funpack -q -ow -apr 6177 "binariseCategorical" output.csv output.csv `

8. Use custom processing function defined in file conversion.py:

`funpack -ow -q -p conversion.py -apr " 20403,20414" "alcohol" -apr " 12144" "meters" -apr "1070" "TV" -apr "894" "TotWalk" -apr " 25781,25010" "WMH" -apr "914,904" "ChamConvert" -apr "894,884" "ChamConvert" -apr "874,864" "ChamConvert" output.csv output.csv`


9. In the end you might want to rename the column with the corresponding name in Whitehall (see file Parser.xslx) as:

`-rc "old name" "new name"`

eg:  funpack -q -ow -rc "31" "GenderM1F0" output.csv output.csv


### AUTOMATED CONVERSION (experimental)

`CONFIGURATION.txt` is the txt file with all the commands, you can pass this file to funpack instead of having to pass all of them separately. 

In the command line you can directly pass the CONFIGURATION file as:

`funpack -cfg CONFIGURATION.txt output.csv ukb26225.csv`  

(look inside the file to understand each step, and decide if and where you need to add new variables or command)



 
