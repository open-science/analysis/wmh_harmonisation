#!/usr/bin/env python

import itertools as it
import numpy as np

from ukbparse import processor

# Cleaner functions are passed:
#
#   - dtable: An object which provides access to the data set.
#   - vid:    The variable ID of the column(s) to be cleaned.
#
# Cleaner functions should modify the data in-place.

@processor()
def meters(dtable, vids): 

    cols   = list(it.chain(*[dtable.columns(v) for v in vids]))
    series  = [dtable[:, c.name] for c in cols]
    conv = sum([s /100 for s in series])
   

    conv.name = 'OX.HEIGHT({})'.format(','.join([str(v) for v in vids]))

    return cols,[conv], None


@processor()
def TV(dtable, vids):

    cols   = list(it.chain(*[dtable.columns(v) for v in vids]))
    series  = [dtable[:, c.name] for c in cols]
    conv = sum([s *7 for s in series])
   

    conv.name = 'TVtot_h/week({})'.format(','.join([str(v) for v in vids]))

    return cols,[conv], None

@processor()
def TotWalk(dtable, vids):

    cols   = list(it.chain(*[dtable.columns(v) for v in vids]))
    series  = [dtable[:, c.name] for c in cols]
    conv = sum([(s*7)/60 for s in series])
   

    conv.name = 'TotWalk_h/week({})'.format(','.join([str(v) for v in vids]))

    return [conv], None

@processor()
def alcohol(dtable, vids):

    cols1 = list(it.chain(*[dtable.columns(v) for v in [vids[0]]]))
    series1 = [dtable[:, c.name] for c in cols1]
    cols2 = list(it.chain(*[dtable.columns(v) for v in [vids[1]]]))
    series2 = [dtable[:, c.name] for c in cols2]
    conv = sum([(s1 * s2) for s1,s2 in zip(series1,series2)])
     
   
    conv.name = 'Alcohol_u/week({})'.format(','.join([str(v) for v in vids]))

  
    return list(it.chain(cols1, cols2)), [conv], None


@processor()
def ChamConvert(dtable, vids):

    cols1 = list(it.chain(*[dtable.columns(v) for v in [vids[0]]]))
    series1 = [dtable[:, c.name] for c in cols1]
    cols2 = list(it.chain(*[dtable.columns(v) for v in [vids[1]]]))
    series2 = [dtable[:, c.name] for c in cols2]
    conv = sum([(s1 * s2)/60 for s1,s2 in zip(series1,series2)])
     
   
    conv.name = 'ChamConversion({})'.format(','.join([str(v) for v in vids]))

    return list(it.chain(cols1, cols2)), [conv], None


@processor()
def WMH(dtable, vids):

    cols1 = list(it.chain(*[dtable.columns(v) for v in [vids[0]]]))
    series1 = [dtable[:, c.name] for c in cols1]
    cols2 = list(it.chain(*[dtable.columns(v) for v in [vids[1]]]))
    series2 = [dtable[:, c.name] for c in cols2]
    conv = sum([(s1 /s2)*100 for s1,s2 in zip(series1,series2)])
     
   
    conv.name = 'WMH_percentage({})'.format(','.join([str(v) for v in vids]))

    return [conv], None















